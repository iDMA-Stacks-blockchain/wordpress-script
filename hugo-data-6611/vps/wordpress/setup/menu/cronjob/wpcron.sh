#!/bin/bash

######################################################################
#           Auto Install & Optimize LEMP Stack on CentOS 7, 8        #
#                                                                    #
#                                                                    #
#                  Website: https://Gafaba.com                       #
#             Dien Dan Thao Luan: https://www.diendantinhoc.vn.      #
#                                                                    #
#              Please do not remove copyright. Thank!                #
#  Please do not copy under any circumstance for commercial reason!  #
######################################################################

if [[ "$(ls -A /var/gafaba/wpcron)" ]]; then
   for domains in /var/gafaba/wpcron/* ; do
        domain=${domains##*/}
        wget -q -O - https://"${domain}"/wp-cron.php?doing_wp_cron >/dev/null 2>&1
    done
fi
